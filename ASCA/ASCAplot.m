function ASCAplot(ASCA,factor,varargin)
%
%
% ASCAplot(ASCA,factor,varargin)
%
%
% Modified by Carlo Bertinetto on 25 September 2019:
%   - Order of colors changed

% INPUT:
% ASCA      - resulting structure from asca.m (Universiteit van Amsterdam:
%           http://www.bdagroup.nl/downloads/bda_downloads.html).
%
% Factor    - The to be plotted factor. If 'factor' exceeds the amount of
%           factors present in present in the ASCA struc, interactions
%           are plotted instead (for the nth interaction use n + thenumber
%           of factors). If 'factor' exceeds the amount of factors + the
%           amount of two factor interactions, a PCA model is made of
%           the residuals (in which the classes are determined in the same
%           way as the two way interactions, in the same order).
%
% Variable INPUT:
% Additional (optional) inputs can be used with the 'specifier' and
% variable combination. These specifier, variable combinations can be used
% in any order. Specifiers are not case sensitive.
% (Example: ASCAplot(ASCA,3,'selec',[1,2,3,4],'varnames',vars))
%
%
% 'Selec'       - Selection of Loadings to be plotted, Default is no loadings.
%               (vector of the to be plotted Loadings, fraction of the to
%               be plotted Loadings or reduce Loadings by a factor of Selec)
%
% 'Varnames'    - Names of the variables (only applicable in combination
%               with 'Selec').
%
% 'Legpos'      - Legend position
%
% 'Classnames'  - Cell array containing the names of the primary classes
%               (only applicable for factor plots) Format:1 by n cell array
%               in which n is the number of levels in 'Factor'.
%
% 'Factor2'      - Uses different symbols to indicate the samples from this
%               factor. The main effect is indicated (the centeres of each
%               of the subgroups should overlap, but the spread can be
%               different.)(used directly in the function to define
%               'Class2')(only applicable for factor plots)
%
% 'Classnames2' - Cell array containing the names of the secondary classes
%               associated with 'Factor2'
%               (only applicable in combination with 'Factor2' and for
%               interaction/residual plots). Format:1 by n cell array in
%               which n is the number of levels in 'Factor2'.
%
% 'Axlabels'    - Custom x and y labels
%
% 'PCs'         - Row or column vector containing the PC's to be plotted.
%               (no input equals the vector [1,2] (or [1;2]))
%
% 'InterDes'    - Two factor interactions that have been included in the
%               ASCA model. (not nescessary if ALL two factor interactions
%               are included, if "ASCA.inter" contains this information or
%               if you are plotting factors instead of interactions. The
%               format is a n by 2 matrix in which each pair represents the
%               two factors which are represented (aka [1 2; 1 3; 2 4]).
%
% 'Trans'       - Controls the transparancy of the legend, 1=solid 0=fully
%               transparant. Default value is 1.
%
% Author: Dirk Jeukens, Radboud University

nr_factor=size(ASCA.factors.loadings,1);
if isfield(ASCA,'interactions')
    nr_inter=min(size(ASCA.interactions.loadings,1),(nr_factor^2-nr_factor)/2);
else
    nr_inter=0;
end
if factor<=nr_factor
    LDS         = ASCA.factors.loadings{factor};
    SCR         = ASCA.factors.scores{factor};
    PRO         = ASCA.factors.projected{factor};
    VAR         = ASCA.factors.explained{factor};
    Class       = ASCA.design(:,factor);
    plotint     = 0;
elseif factor<=nr_factor+nr_inter
    factor=factor-nr_factor;
    LDS         = ASCA.interactions.loadings{factor};
    SCR         = ASCA.interactions.scores{factor};
    PRO         = ASCA.interactions.projected{factor};
    VAR         = ASCA.interactions.explained{factor};
    plotint     = 1;
else
    factor=min(factor-nr_factor-nr_inter,nr_inter);
    if isfield(ASCA,'E')
        data=ASCA.E;
    elseif isfield(ASCA,'residuals')
        data=ASCA.residuals;
    else
        error('Unexpected option: interactions or factor not found')
    end
    [u, sv, LDS] = svd(data);
    SCR = u*sv;
    PRO = zeros(size(SCR));
    VAR = (diag(sv.^2))/sum(sv(:).^2)*100;
    plotint = 2;
end



%predefine optional inputs

Class2=[];
Legpos='best';
Selec=[];
classnames=[];
classnames2=[];
axlabels=[];
varnames=1:size(LDS,1);
colormode=1;
C1=1;
C2=2;
Cstart=1;
Sstart=1;
trans=1;

% 12 unique colors
Mcol= [0 0.75 0;    %green
    1 0.5 0      %orange
    1 0 0        %red
    0 0 1        %blue
    0 1 1        %aqua
    0 1 0        %lime
    0 0 0        %black
    1 0 1        %fuchsia
    0.75 0 0     %maroon
    0.75 0.75 0  %olive
    0 0 0.75     %navy
    0.75 0 0.75];%purple

% Mcol= [1 0 0     %red
%     0 0.75 0;    %green
%     1 0.5 0      %orange
%     0 0 1        %blue
%     0 1 0        %lime
%     0 0 0        %black
%     1 0 1        %fuchsia
%     0 1 1        %aqua
%     0.75 0 0     %maroon
%     0.75 0.75 0  %olive
%     0 0 0.75     %navy
%     0.75 0 0.75];%purple

%11 unique markers
Mshape=['+';'o';'*';'s';'d';'x';'^';'v';'>';'<';'h'];


%evaluate and redefine optional inputs
while ~isempty(varargin)
    switch lower(varargin{1})
        case 'legpos'
            Legpos=varargin{2};
        case 'classnames'
            classnames=varargin{2};
        case 'classnames2'
            classnames2=varargin{2};
        case 'axlabels'
            axlabels=varargin{2};
        case 'factor2'
            Class2 = ASCA.design(:,varargin{2});
        case 'selec'
            Selec=varargin{2};
            if size(Selec)==ones(1,2)
                if Selec<1
                    Selec=Selec^-1*(1:size(LDS,1)*Selec);
                else
                    Selec=Selec*(1:size(LDS,1)*Selec^-1);
                end
            end
        case 'varnames'
            varnames=varargin{2};
        case 'pcs'
            C1=varargin{2}(1);
            C2=varargin{2}(2);
        case 'interdes'
            interdes=varargin{2};
        case 'colormode'
            colormode=varargin{2};
        case 'cstart'
            Cstart=varargin{2};
            if Cstart<1
                Cstart=1;
            end
        case 'sstart'
            Sstart=varargin{2};
            if Sstart<1
                Sstart=1;
            end
        case 'mcol'
            Mcol=varargin{2};
        case 'mshape'
            Mshape=varargin{2};
        case 'trans'
            trans=varargin{2};
        otherwise
            error(['Unexpected option: ' varargin{1}])
    end
    varargin(1:2) = [];
end

if plotint>=1
    if exist('interdes','var')
        Class = ASCA.design(:,interdes(factor,1));
        Class2 = ASCA.design(:,interdes(factor,2));
    elseif isfield(ASCA,'inter')
        Class = ASCA.design(:,ASCA.inter(factor,1));
        Class2 = ASCA.design(:,ASCA.inter(factor,2));
    elseif (nr_factor^2-nr_factor)/2>=nr_inter
        i=1;
        j=factor;
        while j>nr_factor-i
            j=j-(nr_factor-i);
            i=i+1;
        end
        j=j+i;
        [a,b]=meshgrid(1:nr_factor);
        Class = ASCA.design(:,b(i,j));
        Class2= ASCA.design(:,a(i,j));
    else
        error('Unexpected option: unknown interaction design')
    end
end

% check class information:
Classes = unique(Class);
nr_classes = length(Classes);
Classes2 = unique(Class2);
nr_classes2 = length(Classes2);
combclass=[Class,Class2];
Uclasses=unique(combclass,'rows');
totclasses=length(Uclasses);

xscore=1.1*(max(abs(SCR(:,C1)+PRO(:,C1))));
yscore=1.1*(max(abs(SCR(:,C2)+PRO(:,C2))));


figure('Position',[380 50 600 600]);
daspect([xscore,yscore,1])
axis([-xscore,xscore,-yscore,yscore])

% generate class labels:


V=version('-release'); %check if Matlab version is high enough to use multiple columns in legend
V=str2double(V(4));
leg=[];
if plotint==0
    hold on
%     for ind=1:totclasses
%         i=find(Uclasses(:,1)==Uclasses(ind,1));
%         j=find(Uclasses(:,end)==Uclasses(ind,end));
%         
%         group_nr = Uclasses(ind,1);
%         group_nr2 = Uclasses(ind,end);
%         pos_group = find(combclass(:,1) == group_nr & combclass(:,end) == group_nr2);
%         if find(Uclasses(:,1)==i,1)==ind
%             plot(SCR(pos_group(1),C1),SCR(pos_group(1),C2),'color',Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'marker','o','linewidth',1.2, 'MarkerFaceColor', Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'linestyle','none');
%         end
%         plot(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),'color',Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'linestyle','none');
%         %             if isempty(classnames)
%         %                 leg=[leg;cellstr(['Center ',int2str(i)]);cellstr(['Class ',int2str(i)])]; %substitute classnames
%         %             else
%         %                 leg=[leg;cellstr(['Center ',cell2mat(classnames(i))]);classnames(i)];
%         %             end
%         if isempty(classnames) || isempty(classnames2)
%             leg=[leg;cellstr(['Center ',int2str(Uclasses(i,1)),' ',int2str(Uclasses(i,end))])];
%         else
%             leg=[leg;cellstr([cell2mat(classnames(i)),' ',cell2mat(classnames2(j))])];
%         end
%         
%     end
%     if isempty(classnames)
%         for i=1:group_nr
%             classnames{i}=['Class ',num2str(i)]; %substitute names for primary class
%         end
%     end
%     if isempty(classnames2)
%         for i=1:group_nr2
%             classnames2{i}=['Group ',num2str(i)]; %substitute names for secondary class
%         end
%     end
%     hold off
%     if V>=8 && length(Uclasses)==nr_classes*nr_classes2
%         lgd=legend([repmat(cellstr('|'),1,(nr_classes-1)*(nr_classes2+1)),'Center',classnames2],'NumColumns',nr_classes,'AutoUpdate','off');
%         lgd.Title.String =cell2mat([strcat(reshape([repmat({'     '},nr_classes,1),classnames']',1,2*nr_classes)),{'      Group'}]);
%     elseif V>=7
%         lgd=legend(leg,'Location',Legpos,'AutoUpdate','off');
%     else
%         lgd=legend(leg,'Location',Legpos);
%     end
    
    
    
        if isempty(Class2)
            hold on
            for k= 1:nr_classes
                group_nr = Classes(k);
                pos_group = find(Class == group_nr);
                plot(SCR(pos_group(1),C1),SCR(pos_group(1),C2),'color',Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker','o','linewidth',1.2, 'MarkerFaceColor', Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'linestyle','none');
                plot(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),'color',Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+k-2,length(Mshape))),'linestyle','none');
                if isempty(classnames)
                    leg=[leg;cellstr(['Center ',int2str(k)]);cellstr(['Class ',int2str(k)])]; %substitute classnames
                else
                    leg=[leg;cellstr(['Center ',cell2mat(classnames(k))]);classnames(k)];
                end
            end
            hold off
%             if V>=8
%                 lgd=legend(leg,'Location',Legpos,'NumColumns',nr_classes,'AutoUpdate','off');
%             if V>=7
%                 lgd=legend(leg,'Location',Legpos,'AutoUpdate','off');
%             else
                lgd=legend(leg,'Location',Legpos);
%             end
        else
            hold on
            for i=1:nr_classes
                group_nr = Classes(i);
                pos_group = find(Class == group_nr);
                plot(SCR(pos_group(1),C1),SCR(pos_group(1),C2),'color',Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'marker','o','linewidth',1.2, 'MarkerFaceColor', Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'linestyle','none');
                for j=1:nr_classes2
                    group_nr2= Classes2(j);
                    pos_group = find(Class == group_nr & Class2 == group_nr2);
                    plot(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),'color',Mcol(1+rem(Cstart+i-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'linestyle','none');
                    if isempty(classnames) || isempty(classnames2)
                        if j==1
                            leg=[leg;cellstr(['Center ',int2str(Classes(i))])];
                        end
                        if ~isempty(pos_group)
                            leg=[leg;cellstr([int2str(Classes(i)),'-',int2str(Classes2(j))])];
                        end
                    else
                        if j==1
                            leg=[leg;cellstr(['Center ',cell2mat(classnames(i))])];
                        end
                        if ~isempty(pos_group)
                            leg=[leg;cellstr([cell2mat(classnames(i)),'-',cell2mat(classnames2(j))])];
                        end
                    end
                end
            end
            hold off
            if isempty(classnames)
                for i=1:group_nr
                    classnames{i}=['Class ',num2str(i)]; %substitute names for primary class
                end
            end
            if isempty(classnames2)
                for i=1:group_nr2
                    classnames2{i}=['Group ',num2str(i)]; %substitute names for secondary class
                end
            end
            if V>=8 && length(Uclasses)==nr_classes*nr_classes2
                lgd=legend([repmat(cellstr('|'),1,(nr_classes-1)*(nr_classes2+1)),'Center',classnames2],'NumColumns',nr_classes,'AutoUpdate','off');
                lgd.Title.String =cell2mat([strcat(reshape([repmat({'     '},nr_classes,1),classnames']',1,2*nr_classes)),{'      Group'}]);
            elseif V>=7
                lgd=legend(leg,'Location',Legpos,'AutoUpdate','off');
            else
                lgd=legend(leg,'Location',Legpos);
            end
        end
    
    for k= 1:nr_classes
        group_nr = Classes(k);
        pos_group = find(Class == group_nr);
        if length(pos_group)>2
        PlotConvHull(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),'facecolor',Mcol(1+rem(Cstart+k-2,length(Mcol)),:));
        end
    end
    
elseif plotint>=1
    hold on
    if colormode==0 || strcmp(colormode,'low')
        h=scatter(PRO(:,C1)+SCR(:,C1), PRO(:,C2)+SCR(:,C2),[],'k','MarkerEdgeAlpha',.2);
        set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    else
        for ind=1:length(Uclasses)
            k=find(Classes(:)==Uclasses(ind,1),1);
            j=find(Classes2(:)==Uclasses(ind,end),1);
            group_nr = Classes(k);
            group_nr2 = Classes2(j);
            pos_group = find(Class == group_nr & Class2 == group_nr2);
            if plotint==1
                scatter(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),36,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'MarkerEdgeAlpha',.25);
            else
                scatter(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),36,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),Mshape(1+rem(Sstart+j-2,length(Mshape))));
            end
        end
        %                 for k= 1:nr_classes
        %                     group_nr = Classes(k);
        %                     for j=1:nr_classes2
        %                         group_nr2= Classes2(j);
        %                         pos_group = find(Class == group_nr & Class2 == group_nr2);
        %                         if plotint==1
        %                             scatter(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),36,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'MarkerEdgeAlpha',.7);
        %                         else
        %                             scatter(PRO(pos_group,C1)+SCR(pos_group,C1), PRO(pos_group,C2)+SCR(pos_group,C2),36,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),Mshape(1+rem(Sstart+j-2,length(Mshape))));
        %                         end
        %                     end
        %
        %                 end
    end
    
    for ind=1:length(Uclasses)
        k=find(Classes(:)==Uclasses(ind,1),1);
        j=find(Classes2(:)==Uclasses(ind,end),1);
        group_nr = Classes(k);
        group_nr2 = Classes2(j);
        pos_group = find(Class == group_nr & Class2 == group_nr2);
        if plotint==1 && ~isempty(pos_group)
            h=scatter(SCR(pos_group(1),C1),SCR(pos_group(1),C2),75,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'linewidth',2);
            if ~colormode==0 && ~strcmp(colormode,'low')
                set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
            end
        end
        %             text(SCR(pos_group(1),C1)+xscore/20,SCR(pos_group(1),C2)+yscore/20,[num2str(Classes(k)),'-',num2str(Classes2(j))],'Color',Mcol(1+rem(Cstart+k-2,length(Mcol))),'FontSize',12,'HorizontalAlignment','center','VerticalAlignment','baseline')
        if isempty(classnames) || isempty(classnames2)
            leg=[leg;cellstr(['Center ',int2str(Classes(k)),' ',int2str(Classes2(j))])];
        else
            leg=[leg;cellstr([cell2mat(classnames(k)),' ',cell2mat(classnames2(j))])];
        end
    end
    
    
    %         for k= 1:nr_classes
    %             group_nr = Classes(k);
    %             for j=1:nr_classes2
    %                 group_nr2= Classes2(j);
    %                 pos_group = find(Class == group_nr & Class2 == group_nr2);
    %                 if plotint==1 && ~isempty(pos_group)
    %                     h=scatter(SCR(pos_group(1),C1),SCR(pos_group(1),C2),75,Mcol(1+rem(Cstart+k-2,length(Mcol)),:),'marker',Mshape(1+rem(Sstart+j-2,length(Mshape))),'linewidth',2);
    %                     set(get(get(h,'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
    %                 end
    %                 %             text(SCR(pos_group(1),C1)+xscore/20,SCR(pos_group(1),C2)+yscore/20,[num2str(Classes(k)),'-',num2str(Classes2(j))],'Color',Mcol(1+rem(Cstart+k-2,length(Mcol))),'FontSize',12,'HorizontalAlignment','center','VerticalAlignment','baseline')
    %                 if isempty(classnames) || isempty(classnames2)
    %                     leg=[leg;cellstr(['Center ',int2str(Classes(k)),' ',int2str(Classes2(j))])];
    %                 else
    %                     leg=[leg;cellstr([cell2mat(classnames(k)),' ',cell2mat(classnames2(j))])];
    %                 end
    %             end
    %
    %         end
    hold off
    
    if V>=8 && length(Uclasses)==nr_classes*nr_classes2
        
        if isempty(classnames)
            for i=1:nr_classes
                classnames{i}=['Class ',num2str(Classes(i))]; %substitute names for primary class
            end
        end
        if isempty(classnames2)
            for i=1:nr_classes2
                classnames2{i}=['Group ',num2str(Classes2(i))]; %substitute names for secondary class
            end
            
        end
        lgd=legend([repmat(cellstr('|'),1,(nr_classes-1)*(nr_classes2)),classnames2],'NumColumns',nr_classes,'AutoUpdate','off');
        lgd.Title.String =cell2mat([strcat(reshape([repmat({'     '},nr_classes,1),classnames']',1,2*nr_classes)),{'      Group'}]);
        
    elseif V>=7
        lgd=legend(leg,'Location',Legpos,'AutoUpdate','off');
    else
        lgd=legend(leg,'Location',Legpos);
    end
end



if isempty(axlabels)==0
    Xlabel=axlabels(1,:);
    Ylabel=axlabels(2,:);
else
    Xlabel = ['PC ',int2str(C1),' (', int2str(VAR(C1)),'%)'];
    Ylabel = ['PC ',int2str(C2),' (', int2str(VAR(C2)),'%)'];
end
xlabel(Xlabel);
ylabel(Ylabel);

if trans<1
    set(lgd.BoxFace, 'ColorType','truecoloralpha', 'ColorData',uint8(255*[1;1;1;trans]));
end


if isempty(Selec)
    return
end

if VAR(C2)>10^-25
    xload=1.1*(max(abs(LDS(Selec,C1))));
    yload=1.1*(max(abs(LDS(Selec,C2))));
    rat=min([xscore yscore]./[xload yload]);
    LDS(:,C1)=LDS(:,C1)*rat*0.9;
    LDS(:,C2)=LDS(:,C2)*rat*0.9;

else
    xload=1.1*(max(abs(LDS(Selec,C1))));
    yload=1.1*(max(abs(LDS(Selec,C2))));
    rat1=xscore/xload;
    rat2=yscore/yload;

    LDS(:,C1)=LDS(:,C1)*rat1*0.9;
    LDS(:,C2)=LDS(:,C2)*rat2*0.9;
end


hold on
quiver(zeros(length(Selec),1),zeros(length(Selec),1),LDS(Selec,C1),LDS(Selec,C2),'k','ShowArrowHead','off')
hold off

if iscell(varnames) || isstruct(varnames)
    text(LDS(Selec,C1),LDS(Selec,C2),varnames(Selec),'HorizontalAlignment','center','VerticalAlignment','baseline')
elseif isempty(varnames)
    %in case you do not want the variables to be named
elseif isrow(varnames)
    text(LDS(Selec,C1),LDS(Selec,C2),int2str(varnames(Selec)'),'HorizontalAlignment','center','VerticalAlignment','baseline')
elseif iscolumn(varnames)
    text(LDS(Selec,C1),LDS(Selec,C2),int2str(varnames(Selec)),'HorizontalAlignment','center','VerticalAlignment','baseline')
end



return