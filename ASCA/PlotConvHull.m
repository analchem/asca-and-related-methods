function PlotConvHull(X,Y,varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

EdgeCol = 'none';
FCol = [.7 .7 .7];
Ltype = '-';
FAlpha = 0.2;

while ~isempty(varargin)
    switch lower(varargin{1})
        case 'edgecolor'
            EdgeCol = varargin{2};
        case 'facecolor'
            FCol = varargin{2};
        case 'linestyle'
            Ltype = varargin{2};
        case 'facealpha'
            FAlpha = varargin{2};
    end
    varargin(1:2) = [];
end

X = double(X);
Y = double(Y);
K=convhull(X,Y);
handle=patch(X(K),Y(K),FCol);
set(handle,'FaceAlpha',FAlpha,'EdgeColor',EdgeCol,'LineStyle',Ltype)

