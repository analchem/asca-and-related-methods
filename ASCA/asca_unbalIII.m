function [ASCA] = asca_unbalIII(X,F,N,skipfac)

% input:
% X: response matrix (n x p) (samples x variables)
% F: factor matrix (n x k) (samples x number of factors). Each row of F
% corresponds to one sample and each column corresponds to one factor. E.g.
% if there are two factors, time and colour, the first column could
% correspond to the level of the factor time and the second column to the
% level of the factor colour. If F(3,1)=2 it means that the 3rd sample was
% treated at level 2 for the first factor. If F(3,2)=1 it means that the 3rd
% sample was treated at level 1 for the second factor.
%
% optional input:
% N: struct with the nested structure. '0' correstponds to the mean, '1' to
% the first factor (first column of F) etc. Numbers larger then k (and
% smaller then (k^2-k)/2)) correstpond to the consecutive interactions.
% Unassigned indices are automatically added to an additional group.
% Example for k=3: N={[0 1 2 4] 3}.
% The effect of the mean, the first two factors and their interaction are 
% considered first (together), with the residuals of that calculation the 
% effect of the third factor is calculated. The unassigned indices (5 and 
% 6, interactions with third factor) are calculated with the residuals of
% the previous calculation (if skipint=0).
% skipfac: controls whether or not the unassigned factors of N should be
% included in the calculation of the residuals. 0=do not skip, 1=do skip.
% 
% 10 July 2020: modified by Carlo Bertinetto to perform the right type III
% Sum-of-Squares corrections to the effect matrices.


[n,m] = size(F);

%create design matrix
bla=cell(1,m);
for i=1:m
    bla{i}=F(:,i);
end
glm=encode(X,1,2,bla{:});
D=glm.dmat; %design matrix
Terms=glm.terms; %keep track of which columns of the design matrix correspond to which factors or interactions.
uterms=unique(Terms);

n_factors = length(uterms);

clear ASCA; % Structure with results

ASCA.data           = X;
ASCA.design         = F;
SSQ_X               = ssq(X);
ASCA.SSQ_X          = SSQ_X;
ASCA.D              = D;


if nargin<3
    N={unique(Terms)};
    LN=length(N);
elseif length([N{:}])<n_factors
    LN=length(N);
    if ~ismember(0,[N{:}])
        N(2:end+1)=N;
        N(1)={0};
        LN=LN+1;
    end
    N{size(N,2)+1}=uterms(~ismember(uterms,[N{:}]));
end

if nargin<4
   skipfac=0; 
end
E=X;

Tsplit=cell(size(N,2),1);
Dsplit=cell(size(N,2),1);
B_hat=cell(size(N,2),1);
X_hat=cell(size(N,2),1);

for i=1:size(N,2)
    Tsplit{i}=find(ismember(Terms,N{i}));
    Dsplit{i}=D(:,Tsplit{i});
    B_hat{i}=pinv(Dsplit{i}'*Dsplit{i})*Dsplit{i}'*E;
    X_hat{i}=Dsplit{i}*B_hat{i};
    if skipfac==0 || i<=LN
        E=E-X_hat{i};
    end
end




X_means=cell(n_factors,1);
B_hat_reduced=cell(size(N,2),1);
X_hat_reduced=cell(n_factors,1);
E_reduced=cell(n_factors,1);
SSQ_factors=cell(n_factors,1);

k=1;
for i=unique(Terms)
    split=0;
    f=0;
    while f==0 && split<size(N,2)
        split=split+1;
        f=ismember(i,N{split});
    end
    idx=Terms(Tsplit{split})==i;
    Dr=Dsplit{split}; %Reduced design matrix (all factors but factor i).
    Dr(:,idx)=0;
    B_hat_reduced{k}=pinv(Dr'*Dr)*Dr'*X;
    X_hat_reduced{k}=Dr*B_hat_reduced{k}; %matrix of predicted X values considering all factors but factor i.
    if i == 0
        X_means{k} = repmat(mean(X,1),n,1);
    else
        X_means{k}= X_hat{split}-X_hat_reduced{k};
    end
    E_reduced{k}=X-X_hat_reduced{k};
%     SSQ_factors{k}=ssq(E_reduced{k})-SSQ_Efull;
    SSQ_factors{k}=ssq(X_means{k});
    k=k+1;
end


% if skipfac==1 && LN<length(N)
%     notinc=cell2mat(N(LN:end));
%     notinc=nonzeros(notinc)+1;
%     for i=1:length(notinc)
%         E=E+X_means{notinc(i)};
%     end
% end
SSQ_Efull=ssq(E);

ASCA.X_hat=X_hat;
ASCA.B_hat=B_hat;
ASCA.E=E;
ASCA.SSQ_Efull=SSQ_Efull;
ASCA.X_means=X_means;
ASCA.X_hat_reduced=X_hat_reduced;%ASCA.means + ASCA.X_hat_effect should be equal to ASCA.X_hat (within rounding errors)
ASCA.E_reduced=E_reduced; %residual of the reduced model (i.e. model with all factors but factor i).
ASCA.SSQ_factors=SSQ_factors; %type III sum of squares


if skipfac==0
    perc_effects = effect_explains(SSQ_X, [SSQ_factors{:}], SSQ_Efull);
else
    N(end) = [];
    consid_fact = sort([N{:}]); % factors that were considered in the model
    perc_effects = zeros(1,max(Terms)+2);
    perc_effects(1) = 100*(SSQ_factors{1}/SSQ_X);
    for i = 2:max(Terms)+1
        if any(i-1 == consid_fact)
            perc_effects(i) = 100*(SSQ_factors{i}/SSQ_X);
        end
    end
    perc_effects(end) = 100*(SSQ_Efull/SSQ_X);
end
ASCA.effects = perc_effects;


%Do PCA on level averages for each factor
for factor = 1 : size(F,2)
    [t,s,p] = do_pca(X_means{factor+1});
    projected_data = E*p;       % project residuals on loadings
    ASCA.factors.scores{factor,1}    = t;
    ASCA.factors.loadings{factor,1}  = p;
    ASCA.factors.singular{factor,1}  = s;
    ASCA.factors.projected{factor,1} = projected_data;
    ASCA.factors.explained{factor,1} = pc_explains(s);
end

%Do PCA on interactions
for interaction = 1 : n_factors-size(F,2)-1
    [t,s,p] = do_pca(X_means{interaction+size(F,2)+1});
    projected_data = E*p;       % project residuals on loadings
    ASCA.interactions.scores{interaction,1}    = t;
    ASCA.interactions.loadings{interaction,1}  = p;
    ASCA.interactions.singular{interaction,1}  = s;
    ASCA.interactions.projected{interaction,1} = projected_data;
    ASCA.interactions.explained{interaction,1} = pc_explains(s);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Functions
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    function [scores, singular_values, loadings] = do_pca(D)
        % This function does the work: do PCA through singular value
        % decomposition on input matrix. Returns scores, loadings and
        % singular values.
        [u, sv, v] = svd(D);
        scores = u*sv;
        singular_values = diag(sv);
        loadings = v;
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function perc_explained = pc_explains(sv)
 
    % Function to calculate the percentage of variance explained by each PC
    
    % Input
    % vector with singular values 
    
    % Output
    % vector with percentages explained variance
    
        sv_squared     = sv.^2;     
        total_variance = sum(sv_squared);
        perc_explained = (sv_squared/total_variance)*100;
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function perc_explained_effect = effect_explains(ssq_X, ssq_factors, ...
                                      ssq_residuals)
        
        % Function to calculate the percentage of variance explained by
        % each effect.
        
        % Input
        % sums of squares of the data matrix, the factors and the
        % interactions
        
        % Output
        % vector with percentages explained variance for each effect.
        
        SSQ = [ssq_factors ssq_residuals];
        perc_explained_effect = 100*(SSQ./ssq_X);
    end


end