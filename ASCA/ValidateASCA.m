% Performs permutation test on ASCA result, using Sum-of-squares with type
% III corrections as test statistics. It follows the procedure described in:
% 
% D.J. Vis, J.A. Westerhuis, A.K. Smilde, J. van der Greef, Statistical
% validation of megavariate effects in ASCA, BMC Bioinformatics. 8 (2007), 1�8. 
% 
% Not suitable for nested factors.
% 
% Inputs: 
% - ASCA: output structure from 'asca' or asca_unbalIII'
% - n: number of permutations 
% - (optional) 'FactorNames',fnames: to include a cell variable (fnames) 
%                                   with the names of the factors

function [SSQ,p] = ValidateASCA(ASCA,n,varargin)

% 
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

F=ASCA.design;
X=ASCA.data;


if size(F,2)==1
    glm = encode(X,3,2,F(:,1));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
elseif size(F,2)==2
    glm = encode(X,3,2,F(:,1),F(:,2));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
elseif size(F,2)==3
    glm = encode(X,3,2,F(:,1),F(:,2),F(:,3));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
elseif size(F,2)==4
    glm = encode( X,3,2,F(:,1),F(:,2),F(:,3),F(:,4));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
%     Terms=glm.terms([1:44 57:62]); % only keep the factors and interactions that you are interested in
%     D=glm.dmat(:,[1:44 57:62]);
elseif size(F,2)==5
    glm = encode(X,3,2,F(:,1),F(:,2),F(:,3),F(:,4),F(:,5));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
elseif size(F,2)==6
    glm = encode(X,3,2,F(:,1),F(:,2),F(:,3),F(:,4),F(:,5),F(:,6));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
elseif size(F,2)==7
    glm = encode(X,3,2,F(:,1),F(:,2),F(:,3),F(:,4),F(:,5),F(:,6),F(:,7));
    Terms=glm.terms; %vector that keeps track of which columns of D correspond to different factors or interactions.
    D=glm.dmat;
    nfactors = length(unique(Terms));
else
    error('Check the size of the design matrix')
end

tic
SSQ=zeros(nfactors,n+1);
n_main=size(F,2)+1;
n_interact=nfactors-n_main;
id_int=unique(Terms);
id_int=id_int(n_main+1:end); 
id_main=unique(Terms);
id_main=id_main(1:n_main);


for i=1:n+1
    
    %%%%%%%%% calculate main effects
    SSQ_factors=cell(n_main,1);
    if i==1
        Xp=X;
    else
        Xp=X(randperm(size(X,1))',:);
    end
    
    B_hat=pinv(D'*D)*D'*Xp;
    X_hat=D*B_hat;
    E=Xp-X_hat;
    SSQ_Efull=ssq(E);

    k=1;
    for j=id_main 
        idx=Terms==j;
        Dr=D; %Reduced design matrix (all factors but factor j).
       
%         %fit again without the factor that is tested by recalculating
%         % the other regression coefficients:
%         Dr(:,idx)=[];
%         Br=pinv(Dr'*Dr)*Dr'*Xp;
%         X_hatr=Dr*Br; %matrix of predicted X values considering all other factors.
%         %for most factors this approach does not work because Dr has the
%         %same rank as D and their approximations of X are identical (even
%         %though the regression coefficients B are not).

        %fit again without the factor that is tested WITHOUT RECALCULATING
        %the other regression coefficients:
        Dr(:,idx)=0;
        X_hatr=Dr*B_hat;
        
        E_reduced=Xp-X_hatr;
        SSQ_factors{k}=ssq(E_reduced)-SSQ_Efull;
        k=k+1;
    end
    SSQ(1:n_main,i)=cell2mat(SSQ_factors);

    
    %%%%%%%%% calculate interaction effect SSQs
    SSQ_interactions=cell(n_interact,1);
    
    %Identify parts of the design matrix that correspond to interactions
    %and main effects
    idx_i=Terms>=n_main;
    idx_m=Terms<n_main;
    Di=D(:,idx_i); %only contains interactions
    Dm=D(:,idx_m); %only contains main effets
    
    % remove main effects
    B_hat=pinv(D'*D)*D'*X;
    Bm=B_hat(idx_m,:);
    
    Xm=Dm*Bm;
    Xr=X-Xm; %main effects removed
    
    
          
    %permute data where main effects are removed
    if i==1
        Xp=Xr; %change from Xr to X to permute raw data instead of residuals (where the main effect is removed)
    else
        Xp=Xr(randperm(size(F,1))',:); %change from Xr to X to permute raw data instead of residuals (where the main effect is removed)
    end
    Bi=pinv(Di'*Di)*Di'*Xp; %if Xr is not permuted this will be the same as the bottom rows of B_hat
    
    Xp_hat=Di*Bi;
    Er=Xp-Xp_hat; %residuals of the residual matrix Xr.
    SSQ_Er_full=ssq(Er); %
             
    k=1;
    for j=id_int
        idx=Terms==j;
        Dir=Di; %Reduced design matrix (all factors but factor j).
        Dir(:,idx(idx_i))=0;
        
        X_hatr=Dir*Bi; %matrix of predicted X values considering all other interactions
        E_reduced=Xp-X_hatr;
        SSQ_interactions{k}=ssq(E_reduced)-SSQ_Er_full;
        
%         %test to see if this is the same 
%         Dr=D; %Reduced design matrix (all factors but factor j).
%         Dr(:,idx)=0;
%         X_hat_reduced2{k}=Dr*B_hat; %matrix of predicted X values considering all other factors.
%         E_reduced2{k}=Xp-X_hat_reduced2{k};
%         SSQ_factors2{k}=ssq(E_reduced2{k})-SSQ_Efull;
%         
        k=k+1;
    end
    SSQ(n_main+1:end,i)=cell2mat(SSQ_interactions);
end



toc




if any(strcmp(varargin,'FactorNames'))
    FacName = varargin{find(strcmp(varargin,'FactorNames'))+1};
else
    FacName = cell(1,size(F,2));
    for i = 1:length(FacName)
        FacName{1} = ['Fact. ',num2str(i)];
    end
end

% [ASCA] = ascaAvdD(X,F,1);
count = 1;
labels = {'SS_{means}'};
plabels={'means'};
for i = 1:size(F,2)
    count = count + 1;
    labels{count} = ['SS_{',FacName{i},'}'];
    plabels{count} = FacName{i};
end
for i = 1:size(F,2)-1
    for j = i+1:size(F,2)
        count = count + 1;
        labels{count} = ['SS_{',FacName{i},' x ',FacName{j},'}'];
        plabels{count} = [FacName{i},' x ',FacName{j}];
    end
end
% if size(F,2)==1
%     labels={'SS_{means}',['SS_{',FacName{1},'}']};
%     plabels={'means',FacName{1}};
% elseif size(F,2)==2
%     labels={'SS_{means}','SS_{',FacName{1},'}','SS_{',FacName{2},'}','SS_{',FacName{1},' x ',FacName{2},'}'};
%     plabels={'means',FacName{1},FacName{2},[FacName{1},' x ',FacName{2}]};
% elseif size(F,2)==3
%     labels={'SS_{means}','SS_{',FacName{1},'}','SS_{',FacName{2},'}','SS_{',FacName{3},'}','SS_{',FacName{1},' x ',FacName{2},'}',...
%         'SS_{',FacName{1},' x ',FacName{3},'SS_{',FacName{2},' x ',FacName{3},};
%     plabels={'means',FacName{1},FacName{2},FacName{3},[FacName{1},' x ',FacName{2}],[FacName{1},' x ',FacName{3}],[FacName{2},' x ',FacName{3}]};
% elseif size(F,2)==3
%     labels={'SS_{means}','SS_{',FacName{1},'}','SS_{',FacName{2},'}','SS_{',FacName{3},'SS_{',FacName{4},'}',...
%         'SS_{',FacName{1},' x ',FacName{2},'}','SS_{',FacName{1},' x ',FacName{3},'SS_{',FacName{1},' x ',FacName{4},'}',...
%         'SS_{',FacName{2},' x ',FacName{3},'SS_{',FacName{1},' x ',FacName{2},'}',};
%     plabels={'means',FacName{1},FacName{2},FacName{3},[FacName{1},' x ',FacName{2}],[FacName{1},' x ',FacName{3}],[FacName{2},' x ',FacName{3}]};
% elseif size(F,2)==5
%     labels={'SS_{means}','SS_{fMLF}','SS_{day}','SS_{Statin}','SS_{Gender}','SS_{Distance}',...
%         'SS_{fMLF x day}','SS_{fMLF x Statin}','SS_{fMLF x Gender}','SS_{fMLF x Distance}',...
%         'SS_{day x Statin}','SS_{day x Gender}','SS_{day x Distance}','SS_{Statin x Gender}','SS_{Statin x Distance}','SS_{Gender x Distance}'};
%     plabels={'means','fMLF','d','Statin','Gender','Distance',...
%         'f-d','f-S','f-G','f-km','d-S','d-G','d-km','S-G','S-km','G-km'};
% elseif size(F,2)==6
%     labels={'SS_{means}','SS_{ID}','SS_{fMLF}','SS_{day}','SS_{Statin}','SS_{Gender}','SS_{Distance}','SS_{ID x fMLF}','SS_{ID x day}',...
%         'SS_{ID x Statin}','SS_{ID x Gender}','SS_{ID x Distance}','SS_{fMLF x day}','SS_{fMLF x Statin}','SS_{fMLF x Gender}','SS_{fMLF x Distance}',...
%         'SS_{day x Statin}','SS_{day x Gender}','SS_{day x Distance}','SS_{Statin x Gender}','SS_{Statin x Distance}','SS_{Gender x Distance}'};
%     plabels={'means','ID','fMLF','d','Statin','Gender','Distance','ID-f','ID-d',...
%         'ID-S','ID-G','ID-km','f-d','f-S','f-G','f-km',...
%         'd-S','d-G','d-km','S-G','S-km','G-km'};
% end
    p=zeros(nfactors,1);
for i=1:nfactors
    SSQmodel=SSQ(i,1);
    figure('Name',plabels{i},'NumberTitle','off')
    hold on
    hist(SSQ(i,2:end),50)
    plot(SSQmodel,0,'ro','MarkerSize',6,'LineWidth',6)
    ylabel('Frequency','FontSize',16)
    xlabel(labels{i},'FontSize',16)
    legend('Random permutation','Model')
    set(gca,'FontSize',12)
    tmp=length(find(SSQ(i,2:end)>SSQmodel)); %number of random permutations that have a larger SS than the real model
    p(i)=tmp/n;
end

figure('Name','P values','NumberTitle','off')
bar(p)
set(gca,'XTickLabel',plabels,'FontSize',16)
ylabel('p value','FontSize',16)

end

