function a=ssq(X);

a=sum(sum(X.^2));