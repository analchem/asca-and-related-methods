function plotanovatpsign(factormodel)
% plotanovatpsign Plots ANOVA-TP significance plot
%  The function plots the values of test statistics for significance of 
%  factors (and interactions) effects in ANOVATP, together with their distributions
%  under the null hypothesis, as estimated by permutation tests.
%  Four measure of multivariate effect are calculated: 
%   Sum of squares of the effect matrix
%   Sum of squares of the Y residuals
%   Wilks' lambda
%   Rao's trace.
%  INPUTS:
%   factormodel = field of anova TP model corresponding to the design term
%
%  I/O:
%       plotanovatpsign(factormodel);
%
%
% Author: Federico Marini
% Version: 20/04/2020

figure
hist(factormodel.ANOVATP.NullDistr, 100)
hold on
yax=ylim;
plot([factormodel.ANOVATP.EffectSSQ factormodel.ANOVATP.EffectSSQ], yax, 'r', 'linewidth', 1.5)
title(['Effect of term X',factormodel.EffectLabel,' - p= ', num2str(factormodel.ANOVATP.pNull)], 'fontsize', 12, 'fontweight', 'bold')
xlabel('Sum of squares of Effect Matrix', 'fontsize', 12, 'fontweight', 'bold'), ylabel('Frequency', 'fontsize', 12, 'fontweight', 'bold')
axis tight
shg

figure
hist(factormodel.ANOVATP.ResidDistr, 100)
hold on
yax=ylim;
plot([factormodel.ANOVATP.EffectResiduals factormodel.ANOVATP.EffectResiduals], yax, 'r', 'linewidth', 1.5)
title(['Effect of term X',factormodel.EffectLabel,' - p= ', num2str(factormodel.ANOVATP.pRes)], 'fontsize', 12, 'fontweight', 'bold')
xlabel('Sum of squares of Y Residuals', 'fontsize', 12, 'fontweight', 'bold'), ylabel('Frequency', 'fontsize', 12, 'fontweight', 'bold')
axis tight
shg

figure
hist(factormodel.ANOVATP.LambdaDistr, 100)
hold on
yax=ylim;
plot([factormodel.ANOVATP.EffectLambda factormodel.ANOVATP.EffectLambda], yax, 'r', 'linewidth', 1.5)
title(['Effect of term X',factormodel.EffectLabel,' - p= ', num2str(factormodel.ANOVATP.pLam)], 'fontsize', 12, 'fontweight', 'bold')
xlabel('Wilks'' Lambda', 'fontsize', 12, 'fontweight', 'bold'), ylabel('Frequency', 'fontsize', 12, 'fontweight', 'bold')
axis tight
shg

figure
hist(factormodel.ANOVATP.TraceDistr, 100)
hold on
yax=ylim;
plot([factormodel.ANOVATP.EffectTrace factormodel.ANOVATP.EffectTrace] , yax, 'r', 'linewidth', 1.5)
title(['Effect of term X',factormodel.EffectLabel,' - p= ', num2str(factormodel.ANOVATP.pTr)], 'fontsize', 12, 'fontweight', 'bold')
xlabel('Rao''s Trace', 'fontsize', 12, 'fontweight', 'bold'), ylabel('Frequency', 'fontsize', 12, 'fontweight', 'bold')
axis tight
shg

