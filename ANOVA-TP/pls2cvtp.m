function model=pls2cvtp(X,Y,Amax,pretr,lvsel,cv, tpflag, cvopt)
% pls2cvtp Internal routine for cross-validate PLS2 with TP
%  Calculates PLS2 for designed data with target projection. Selection of
%  the number of component is based on the lowest error in cross-validation
%  and can be manual or automatic.
%  It is called within the anovaTP routine.
% 
% Author: Federico Marini
% Version: 20/04/2020

if strcmp(cv,'on')
    [Aopt,rmsecv]=pls2cv(X,Y,Amax, pretr, lvsel, cvopt);
    
    model=pls2(X,Y,Aopt,pretr,tpflag);
    model.RMSECV=rmsecv;
    
else
    model=pls2(X,Y,Amax,pretr,tpflag);
end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function model=pls2(X,Y,A,pretr,tpflag)

switch pretr{1}
    case 'mean'
        m=mean(X);s=[];
        xxtr=X-repmat(m,size(X,1),1);
    case 'auto'
        m=mean(X); s=std(X);
        xxtr=(X-repmat(m,size(X,1),1))./repmat(s,size(X,1),1);
        
    case 'pareto'
        m=mean(X); s=sqrt(std(X));
        xxtr=(X-repmat(m,size(X,1),1))./repmat(s,size(X,1),1);
end

switch pretr{2}
    case 'mean'
        my=mean(Y); sy=[];
        yytr=Y-repmat(my,size(Y,1),1);
    case 'auto'
        my=mean(Y); sy=std(Y);
        yytr=(Y-repmat(my,size(Y,1),1))./repmat(sy,size(Y,1),1);
    case 'pareto'
        my=mean(Y); sy=sqrt(std(Y));
        yytr=(Y-repmat(my,size(Y,1),1))./repmat(sy,size(Y,1),1);
end

S=(yytr'*xxtr)';

[n,px]=size(xxtr);
[~,py]=size(yytr);

T=zeros(n,A);
U=T;						% initialization of variables
R=zeros(px,A);
P=R;
V=R;
C=zeros(py,A);
z=zeros(py,1);



StS=S'*S;				    % SIMPLS algorithm
nm1=n-1;

for a=1:A
    StS=StS-z*z';
    if any(isnan(StS(:))|isinf(StS(:)))
        % Just to handle errors when too many components are used.
        Q = rand(size(StS));
        LAMBDA = diag(rand(size(StS,1)));
    else
        [Q,LAMBDA]=eig(StS);
    end
    
    [lambda,j]=max(diag(LAMBDA));
    q=Q(:,j(1));
    r=S*q;
    t=xxtr*r;
    
    p=(t'*xxtr)';
    
    if n>px,
        dd=sqrt(r'*p/nm1);
    else
        dd=sqrt(t'*t/nm1);
    end
    
    v=p-V(:,1:max(1,a-1))*(p'*V(:,1:max(1,a-1)))';
    v=v/sqrt(v'*v);
    z=(v'*S)';
    S=S-v*z';
    V(:,a)=v;
    R(:,a)=r/dd; 						    % X weights
    P(:,a)=p/(dd*nm1); 						% X loadings
    T(:,a)=t/dd;							% X scores
    U(:,a)=yytr*q;							% Y scores
    C(:,a)=q*(lambda(1)/(nm1*dd)); 			% Y loadings
    
end



B=R*C';     					        % B-coefficients of the regression Y on X

ypred=xxtr*B;    %prediction on bootstrap sample

switch pretr{2}
    case 'mean'
        yp=ypred+repmat(my,size(ypred,1), 1);
    case 'auto'
        yp=(ypred.*repmat(sy,size(ypred,1), 1))+repmat(my,size(ypred,1), 1);
    case 'pareto'
        yp=(ypred.*repmat(sy,size(ypred,1), 1))+repmat(my,size(ypred,1), 1);
end



%Target projection
ntp=min(size(B,2),A);
Wtp=zeros(size(B,1),ntp);
for i=1:ntp
    
    Wtp(:,i)=B(:,i)./norm(B(:,i));
end


switch tpflag
    case 'corr'
        Ttp=xxtr*Wtp;
        Ptp=pinv(Ttp)*xxtr; 
        Ptp=Ptp';
        
    case 'corrW'
        Ttp=xxtr*Wtp;
        Ptp=Wtp;
        
    case 'orth'
        Ttp=zeros(size(T,1), ntp);
        Ptp=zeros(size(Wtp));
        xxtrd=xxtr;
        
        for i=1:ntp
            Ttp(:,i)=xxtrd*Wtp(:,i);
            Ptp(:,i)=(xxtrd'*Ttp(:,i))./(Ttp(:,i)'*Ttp(:,i));
            xxtrd=xxtrd-Ttp(:,i)*Ptp(:,i)';
        end
        
    case 'orthW'
        Ttp=zeros(size(T,1), ntp);
        Ptp=Wtp;
        xxtrd=xxtr;
        
        for i=1:ntp
            Ttp(:,i)=xxtrd*Wtp(:,i);
            xxtrd=xxtrd-Ttp(:,i)*Ptp(:,i)';
        end
        
end

Ttp1=mscores(Y, Ttp);
Xtp=Ttp1*Ptp';


model.Ypred=yp;
model.nf=A;
model.Xscores=T;
model.Yscores=U;
model.Xweights=R;
model.Xloadings=P;
model.Yloadings=C;
model.regcoef=B;
model.preproc=pretr;
model.preprpars={m s; my sy};
model.TPXscores=Ttp;
model.TPXweights=Wtp;
model.TPXloadings=Ptp;
model.TPXhat=Ttp*Ptp';
model.TPXscoresAve=Ttp1;
model.TPXhatAve=Xtp;
Bx=model.TPXscores'*Y*pinv(Y)*model.TPXscores;
Wx=model.TPXscores'*(eye(size(Y,1))-Y*pinv(Y))*model.TPXscores;
Ax=Wx\Bx;
model.Trace=trace(Ax);
model.Lambda=1./det(Ax);

    





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function T=mscores(y,t)
[~,~,CC]=unique(round(10000*y), 'rows');
T=zeros(size(t));

for i=1:max(CC)
    a1=find(CC==i);
    T(a1,:)=repmat(mean(t(a1,:)), length(a1), 1);
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Aopt, rmsecv]=pls2cv(X,Y,A, pretr, lvsel, cvopt)
ns=size(X,1);
ncv=cvopt.ncv;
cvperc=cvopt.cvperc;

levs=unique(round(100000*Y), 'rows'); 
nl=size(levs,1);
uns=cell(nl,1);

for i=1:nl
    uns{i}=find(ismember(round(100000*Y), levs(i,:), 'rows')==1); 
end


rmsecv=zeros(1,A);
ntot=0;

for j=1:ncv
    ntr=[];
    nts=[];
    for k=1:nl
        np=randperm(length(uns{k}));
        nsel=round(cvperc*length(uns{k}));
        if nsel==0
            nsel=1;
        end
        
        nts=[nts;uns{k}(np(1:nsel))];
        ntr=[ntr;uns{k}(np(nsel+1:end))];
    end
    ntr=sort(ntr); 
    nts=sort(nts);
    
    Xtr=X(ntr,:); 
    Ytr=Y(ntr,:); 
    Xts=X(nts,:); 
    Yts=Y(nts,:); 
    
    
    ssq=pls2loo(Xtr,Ytr,Xts,Yts, A, pretr);
    rmsecv=rmsecv+ssq;
    ntot=ntot+length(nts);
end

rmsecv=sqrt(rmsecv/ntot);    

switch lvsel
    case 'manual'
        figure
        plot(1:A, rmsecv, 'b')
        xlabel('Number of LVs'), ylabel('RMSECV')
        Aopt=input('Select the number of component to retain:  ');
        
    case 'autoF'
        Aopt=selminlv(rmsecv, ns*size(Y,2));
    case 'chisq'
        cvsignificant = chi2cdf(((rmsecv.^2)/(ns*min(rmsecv.^2))).^(-1),ns);
        pvalue = 0.05;
        Aopt  = find(cvsignificant > pvalue,1,'first');
        %minerr=rmsecv(Aopt);
end

%if Aopt<size(Y,2);
%    Aopt=size(Y,2);
%end






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Aopt=selminlv(rmsecv, dof)
[s1,Amin]=min(rmsecv);
Flim=finv(0.95,dof-Amin, dof-Amin)*(s1.^2);
slim=sqrt(Flim);
[~,Aopt]=find(rmsecv<slim, 1);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ssq=pls2loo(Xtr,Ytr,Xts,Yts, A, pretr)

ssq=zeros(1,A);


switch pretr{1}
    case 'mean'
        m=mean(Xtr);
        xxtr=Xtr-repmat(m,size(Xtr,1),1);
        xxts=Xts-repmat(m,size(Xts,1),1);
    case 'auto'
        m=mean(Xtr); s=std(Xtr);
        xxtr=(Xtr-repmat(m,size(Xtr,1),1))./repmat(s,size(Xtr,1),1);
        xxts=(Xts-repmat(m,size(Xts,1),1))./repmat(s,size(Xts,1),1);
        
    case 'pareto'
        m=mean(Xtr); s=sqrt(std(Xtr));
        xxtr=(Xtr-repmat(m,size(Xtr,1),1))./repmat(s,size(Xtr,1),1);
        xxts=(Xts-repmat(m,size(Xts,1),1))./repmat(s,size(Xts,1),1);
end

switch pretr{2}
    case 'mean'
        my=mean(Ytr);
        yytr=Ytr-repmat(my,size(Ytr,1),1);
    case 'auto'
        my=mean(Ytr); sy=std(Ytr);
        yytr=(Ytr-repmat(my,size(Ytr,1),1))./repmat(sy,size(Ytr,1),1);
    case 'pareto'
        my=mean(Ytr); sy=sqrt(std(Ytr));
        yytr=(Ytr-repmat(my,size(Ytr,1),1))./repmat(sy,size(Ytr,1),1);
end

S=(yytr'*xxtr)';

[n,px]=size(xxtr);
[~,py]=size(yytr);

T=zeros(n,A);
U=T;						% initialization of variables
R=zeros(px,A);
P=R;
V=R;
C=zeros(py,A);
z=zeros(py,1);



StS=S'*S;				    % SIMPLS algorithm
nm1=n-1;

for a=1:A
    StS=StS-z*z';
    if any(isnan(StS(:))|isinf(StS(:)))
        % Just to handle errors when too many components are used.
        Q = rand(size(StS));
        LAMBDA = diag(rand(size(StS,1)));
    else
        [Q,LAMBDA]=eig(StS);
    end
    
    [lambda,j]=max(diag(LAMBDA));
    q=Q(:,j(1));
    r=S*q;
    t=xxtr*r;
    
    p=(t'*xxtr)';
    
    if n>px,
        dd=sqrt(r'*p/nm1);
    else
        dd=sqrt(t'*t/nm1);
    end
    
    v=p-V(:,1:max(1,a-1))*(p'*V(:,1:max(1,a-1)))';
    v=v/sqrt(v'*v);
    z=(v'*S)';
    S=S-v*z';
    V(:,a)=v;
    R(:,a)=r/dd; 						    % X weights
    P(:,a)=p/(dd*nm1); 						% X loadings
    T(:,a)=t/dd;							% X scores
    U(:,a)=yytr*q;							% Y scores
    C(:,a)=q*(lambda(1)/(nm1*dd)); 			% Y loadings
    
    B=R*C';     					        % B-coefficients of the regression Y on X
    
    ypred=xxts*B;    %prediction on bootstrap sample
    
    switch pretr{2}
        case 'mean'
            yp=ypred+repmat(my,size(ypred,1), 1);
        case 'auto'
            yp=(ypred.*repmat(sy,size(ypred,1), 1))+repmat(my,size(ypred,1), 1);
        case 'pareto'
            yp=(ypred.*repmat(sy,size(ypred,1), 1))+repmat(my,size(ypred,1), 1);
    end
    
    ssq(a)=sum(sum((yp-Yts).^2));
    
end




