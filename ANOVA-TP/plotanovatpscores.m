function plotanovatpscores(factormodel, tpcomp)
% plotanovatpsign Plots ANOVA-TP scores plot
%  The function plots the values of ANOTATP scores eitehr as a bar plot (if 
%  model includes only one component, or as 2D scatterplot if at least two 
%  components are extracted. 
%  First plot plots only the mean scores for each factor level (to be
%  comparable, e.g., to ASCA scores), while the second one plots the
%  individual TP scores (which can be compared to ASCA after projection of
%  the residuals).
%
%  INPUTS:
%   factormodel = field of anova TP model corresponding to the design term
%  OPTIONAL INPUTS:
%        tpcomp = Vector with the number of components to be plotted (if
%                 empty 1:min(ncomp,2)).
%  I/O:
%       plotanovatpscores(factormodel, tpcomp);
%       plotanovatpscores(factormodel);
%
%
% Author: Federico Marini
% Version: 20/04/2020

if nargin==1 || isempty(tpcomp) ||max(tpcomp)>size(factormodel.ANOVATP.Scores,2)
    tpcomp=1:min(size(factormodel.ANOVATP.Scores,2), 2);
end



[~,~,lindex]=unique(factormodel.DesignMatrix,'rows', 'stable');
Mark={'o','s','d','^','v','>','<','p','h','+','*','.','x'};
colr=lines(7);

if length(tpcomp)>1
    figure
    hold on
    
    for i=1:max(lindex)
        f1=rem(i,13); 
        if f1==0 
            f1=13;
        end
        f2=rem(i,7); 
        if f2==0 
            f2=7;
        end
        
        plot(factormodel.ANOVATP.MeanScores(lindex==i,tpcomp(1)), factormodel.ANOVATP.MeanScores(lindex==i,tpcomp(2)),Mark{f1}, 'markerfacecolor', colr(f2,:), 'markeredgecolor', colr(f2,:), 'markersize', 10)
    end
    
    xx=axis; 
    plot(xx(1:2), [0 0], '--k', 'linewidth', 1.2)
    plot([0 0],xx(3:4),  '--k', 'linewidth', 1.2)
    xlabel(['Scores on TP', num2str(tpcomp(1))], 'fontsize', 12, 'fontweight', 'bold')
    ylabel(['Scores on TP', num2str(tpcomp(2))], 'fontsize', 12, 'fontweight', 'bold')
    
    
    figure
    hold on
    
    for i=1:max(lindex)
        f1=rem(i,13); 
        if f1==0 
            f1=13;
        end
        f2=rem(i,7); 
        if f2==0 
            f2=7;
        end
        
        plot(factormodel.ANOVATP.MeanScores(lindex==i,tpcomp(1)), factormodel.ANOVATP.MeanScores(lindex==i,tpcomp(2)),Mark{f1}, 'markerfacecolor', colr(f2,:), 'markeredgecolor', colr(f2,:), 'markersize', 10)
        plot(factormodel.ANOVATP.Scores(lindex==i,tpcomp(1)), factormodel.ANOVATP.Scores(lindex==i,tpcomp(2)),Mark{f1}, 'markeredgecolor', colr(f2,:), 'markersize', 6)
    end
    
    xx=axis; 
    plot(xx(1:2), [0 0], '--k', 'linewidth', 1.2)
    plot([0 0],xx(3:4),  '--k', 'linewidth', 1.2)
    xlabel(['Scores on TP', num2str(tpcomp(1))], 'fontsize', 12, 'fontweight', 'bold')
    ylabel(['Scores on TP', num2str(tpcomp(2))], 'fontsize', 12, 'fontweight', 'bold')
    
else
    figure
    hold on
    for i=1:max(lindex)
        f2=rem(i,7); 
        if f2==0 
            f2=7;
        end
        
        bar(find(lindex==i), factormodel.ANOVATP.MeanScores(lindex==i,tpcomp(1)),'facecolor', colr(f2,:), 'edgecolor', colr(f2,:))
    end
    
    axis tight
    xlabel('Sample Index', 'fontsize', 12, 'fontweight', 'bold')
    ylabel(['Scores on TP', num2str(tpcomp(1))], 'fontsize', 12, 'fontweight', 'bold')
    
    figure
    hold on
    for i=1:max(lindex)
        f2=rem(i,7); 
        if f2==0 
            f2=7;
        end
        
        bar(find(lindex==i), factormodel.ANOVATP.Scores(lindex==i,tpcomp(1)),'facecolor', colr(f2,:), 'edgecolor', colr(f2,:))
    end
    
    axis tight
    xlabel('Sample Index', 'fontsize', 12, 'fontweight', 'bold')
    ylabel(['Scores on TP', num2str(tpcomp(1))], 'fontsize', 12, 'fontweight', 'bold')
    
end

        
        