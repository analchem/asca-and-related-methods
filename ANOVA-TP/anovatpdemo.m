%% Load the data set

% The only input needed are a data matrix X and a design matrix with the
% values of the level of the main factors. It is not needed that the values
% be numerically consecutive, e.g., from 1 to L, they just need to be
% different. 
% For the present data set, the data are collected in the matrix Xc and the
% corresponding design is in the variable dc.

load coffeedata
%% load the default options

opt=anovaTP('options'); 


%% Modify the options that you wish: 

% In case you do not want bootstrapping to estimate the confidence intervals of the
% TP loadings then, uncomment the following line.
%opt.bootstrap='off'; 

% In case you do not want plots to be displayed during modeling (plots can be
% drawn in a later stage by using specific plotting functions (see later)
% then, uncomment the following line. 
%opt.plots='off'; 
 
% Manual selection of the optimal number of latent variable for each design
% term
opt.lvsel='manual'; 

%% Calculate the model

% The output model structure contains information on the structure and contribution 
% of the different design terms. Within the substructure corresponding to
% the specific model terms, one can find information about the effect
% matrix, the effect sum of squares and explained variance but also about the
% significance of the effect (the results of the permutation tests for the different
% test statistics (SSQ of the effect matrix, SSQ of the Y residuals, Wilks' lambda and Rao's trace
% are stored in the same substructure). Moreover, if bootstrap is
% performed, also the results of bootstrapping are saved there. 
% Therefore, to know about the contribution of factor A, you should look at
% m_tp.XA and its subfield m_tp.XA.ANOVATP and so on.
% 
m_tp=anovaTP(Xc, dc, opt); 


%% Plotting the significance of each factor

% In order to plot the comparison between the test statistics for factor effect 
% (i.e., SSQ of the Effect matrix, SSQ of the Y residuals, Wilks' lambda or Rao's trace) 
% and the corresponding null distribution (together with estimated p value.
% use the function plotanovatpsign. It takes as argument the factor
% submodel (e.g., m_tp.XB) for factor B

plotanovatpsign(m_tp.XB)

% for factor A uncomment the following: 
% plotanovatpsign(m_tp.XA) 
% and for interaction AB
% plotanovatpsign(m_tp.XAB) 

%% Plotting the scores

% A similar function is available for plotting the TP scores. 
% First plot plots only the mean scores for each factor level (to be
% comparable, e.g., to ASCA scores), while the second one plots the
% individual TP scores (which can be compared to ASCA after projection of
% the residuals). 
% The function plotanovatpscores, takes as argument the design term
% submodel (e.g., m_tp.XA) for factor A

plotanovatpscores(m_tp.XA)

% for factor B uncomment the following: 
% plotanovatpscores(m_tp.XB)
% and for interaction AB
% plotanovatpscores(m_tp.XAB) 

%% Plotting the TP loadings

% Finally, if bootstrap has been used during model building to evaluate the
% significance of the variable contributions to the TP loadings, the
% loadings of the variables, together with their confidence intervals, can
% be plotted either as bar plot (using plotbartploads) or as line
% (spectral-like) plots (using plottpspecloads). 
%
% The syntax of both functions is exactly the same (apart from the name)
% Here, due to the high number of variables, I recommend to use
% spectral-like plot:

plottpspecloads(m_tp.XA, wn, 'Wavenumber (cm^{-1})')