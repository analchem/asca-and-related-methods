function model=anovaTP(varargin)
% anovaTP Calculates an ANOVA-TP model on designed data
%  The function anovaTP calculates an ANOVA-Target Projection (ANOVA-TP)
%  model for the analysis of multivariate data resulting from a designed
%  experiment. 
%
%  INPUT: 
%            X = Matrix collecting the experimental data
%    desmatrix = Matrix of the experimental design (levels should be
%                coded numerically (even if not necessarily
%                consequentially). 
%  OPTIONAL INPUT 
%          opt = Structure collecting the options for training the model
%                Contains the following fields.
%            LV: Maximum number of latent variables in PLS-TP (default = 5)
%  reducedmodel: [{'standard'} | 'none'] Defines the calculation of the
%                reduced ANOVA model for modeling and testing
%      permtest: [{'on'} | 'off'] Decides if permutation tests have to be
%                performed or not for factor significance
%         nperm: Number of permutations (default = 10000)
%         plots: [{'on'} | 'off'] Regulates plotting
%     bootstrap: [{'on'} | 'off'] Decides if bootstrap of factor TP loadings has to be
%                performed or not for evaluation of variable contributions
%         nboot: Number of bootstrap repetitions (default = 10000)
%      bootsave: ['all' | {'confint'} | 'signvars'] Regulates the amount of
%                bootstrap results saved in the model.
%         pretr: 1x2 cell array defining the pretreatment of X and Y matrices for 
%                PLS-TP (default is {'mean' 'mean'})
% .       lvsel: ['manual' | {'chisq'} | 'autoF'] Decides how the optimal
%                number of LVs is selected based on the CV results
%                (manually or automatically on the basis of either a chi
%                square or an F test).
%       Ymatrix: [{'pca'} | 'original'] Defines whether the original
%                sum-to-zero coded design matrix or its svd should be used as input
%                for PLS-TP
%        TPproj: ['corr' |{'orth'}| 'corrW' | 'orthW'] Regulates the
%                extraction of TP components and their correlation. Values
%                indicate, respectively, correlated TP components,
%                orthogonalized TP components and their sMC equivalents.
%            cv: Defines the details of Monte-Carlo CV of PLS-TP models
%                (its subfields are the percentage of samples in each
%                cancelation group, cvperc, and the number of iterations, ncv)
%                Defaults are 0.20 and 100 (i.e., 100 iterations, with 20%
%                of the samples in each cancelation group).
%           
%       Default options can be obtained by typing: 
%       opt=anovaTP('options'); 
%     
% . OUTPUT: 
%        model = Structure with the final ANOVA-TP model
%
%   I/O:
%      model=anovaTP(X, desmatrix, opt); 
%      model=anovaTP(X, desmatrix); 
%
%
% Author: Federico Marini
% Version: 20/04/2020



if nargin==1 && strcmp(varargin{1}, 'options')
    
    opt.LV=5;
    opt.reducedmodel='standard';
    opt.permtest='on';
    opt.nperm=10000;
    opt.plots='on';
    opt.bootstrap='on'; 
    opt.nboot=1000;
    opt.bootsave='confint';
    opt.pretr={'mean' 'mean'};
    opt.lvsel='chisq';
    opt.Ymatrix='pca';
    opt.TPproj='orth';
    opt.cv.cvperc=0.20;
    opt.cv.ncv=100;
    
    
    model=opt;
    return
    
elseif nargin==2
    X=varargin{1};
    desmatrix=varargin{2};
    opt.LV=5;
    opt.reducedmodel='standard';
    opt.permtest='on';
    opt.nperm=10000;
    opt.plots='on';
    opt.bootstrap='on'; 
    opt.nboot=1000;
    opt.bootsave='confint';
    opt.pretr={'mean' 'mean'};
    opt.lvsel='chisq';
    opt.Ymatrix='pca';
    opt.TPproj='orth';
    opt.cv.cvperc=0.20;
    opt.cv.ncv=100;
    
elseif nargin==3
    X=varargin{1};
    desmatrix=varargin{2};
    opt=varargin{3};
end


model.Design=desmatrix;
dmain=gendmat(desmatrix);
[dmatrices,desterms,deslabels, desorder]=createdesign(dmain);

Xd=X;

if length(opt.LV)==1
    facts=repmat(opt.LV,1,length(dmatrices)-1);
else
    facts=opt.LV;
end


for i=1:length(dmatrices);
    Xeff=dmatrices{i}*pinv(dmatrices{i})*Xd;
    ssqEff=sum(sum(Xeff.^2));
    Xd=Xd-Xeff;
    l=strtrim(deslabels{i});
    eval(['model.X', l,'.ANOVA.EffectMatrix=Xeff;'])
    eval(['model.X', l,'.ANOVA.EffectSSQ=ssqEff;'])
    
    if i==1
        ssqtot=sum(sum(Xd.^2));
        model.Xdata.CenteredData=Xd;
        model.Xdata.CenteredSSQ=ssqtot;
    else
        expVar=100*(ssqEff/ssqtot);
        eval(['model.X', l,'.ANOVA.EffectExplVar=expVar;'])
    end
    
    eval(['model.X', l,'.DesignMatrix=dmatrices{i};'])
    eval(['model.X', l,'.DesignTerms=desterms{i};'])
    eval(['model.X', l,'.EffectLabel=strtrim(deslabels{i});'])
    eval(['model.X', l,'.TermOrder=desorder(i);'])
    
end

model.XRes.ANOVA.EffectMatrix=Xd;
model.XRes.ANOVA.EffectSSQ=sum(sum(Xd.^2));
model.XRes.ANOVA.EffectExplVar=100*(model.XRes.ANOVA.EffectSSQ/ssqtot);
model.XRes.EffectLabel='Res';
model.XRes.TermOrder=max(desorder)+1;

for i=2:length(dmatrices);
    l=strtrim(deslabels{i});
    
    if ischar(opt.reducedmodel) && strcmp(opt.reducedmodel, 'standard');
        remfact=desterms([2:i-1 i+1:end]);
    elseif ischar(opt.reducedmodel) && strcmp(opt.reducedmodel, 'none');
        remfact=[];
    else
        remfact=opt.reducedmodel{i};
    end
    
    Xx=model.Xdata.CenteredData;
    
    if ~isempty(remfact)
        
        for j=1:length(remfact);
            m=strtrim(char(64+remfact{j}));
            eval(['Xx=Xx-model.X',m,'.ANOVA.EffectMatrix;'])
        end
    end
    
    eval(['model.X', l,'.ReducedMatrix=Xx;'])
    
    Yf=createdesY(dmatrices{i}, opt.Ymatrix);
    mF=pls2cvtp(Xx,Yf,facts(i-1),opt.pretr,opt.lvsel, 'on',opt.TPproj, opt.cv);
    ssqF=sum(sum((mF.TPXhatAve).^2));
    resF=sum(sum((Yf-mF.Ypred).^2));
    trF=mF.Trace;
    lamF=mF.Lambda;
    
    
    
    eval(['model.X', l,'.ANOVATP.EffectMatrix=mF.TPXhatAve;'])
    eval(['model.X', l,'.ANOVATP.LVOpt=mF.nf;'])
    eval(['model.X', l,'.ANOVATP.Scores=mF.TPXscores;'])
    eval(['model.X', l,'.ANOVATP.MeanScores=mF.TPXscoresAve;'])
    eval(['model.X', l,'.ANOVATP.Loadings=mF.TPXloadings;'])
    eval(['model.X', l,'.ANOVATP.EffectSSQ=ssqF;'])
    eval(['model.X', l,'.ANOVATP.EffectResiduals=resF;'])
    eval(['model.X', l,'.ANOVATP.EffectTrace=mF.Trace;'])
    eval(['model.X', l,'.ANOVATP.EffectLambda=mF.Lambda;'])
    
    if strcmp(opt.plots, 'on')
        figure
        plot(1:size(mF.TPXhatAve,2), mF.TPXhatAve')
        title(['Effect matrix for term X',l])
        xlabel('Variable Index')
        axis tight
    end
    
    
    if strcmp(opt.permtest, 'on')
        
        
        SF=zeros(opt.nperm,1);
        RF=SF;
        LF=SF;
        TF=SF;
        
        for k=1:opt.nperm
            a=randperm(size(Xx,1));
            mp=pls2cvtp(Xx,Yf(a,:),mF.nf,opt.pretr,[],[],opt.TPproj, opt.cv);
            SF(k)=sum(sum((mp.TPXhatAve).^2));
            RF(k)=sum(sum((Yf(a,:)-mp.Ypred).^2));
            LF(k)=mp.Lambda;
            TF(k)=mp.Trace;
        end
        eval(['model.X', l,'.ANOVATP.NullDistr=SF;'])
        pNullF=length(find(SF>ssqF))/opt.nperm;
        eval(['model.X', l,'.ANOVATP.pNull=pNullF;'])
        eval(['model.X', l,'.ANOVATP.ResidDistr=RF;'])
        pResF=length(find(RF<resF))/opt.nperm;
        eval(['model.X', l,'.ANOVATP.pRes=pResF;'])
        eval(['model.X', l,'.ANOVATP.LambdaDistr=LF;'])
        pLamF=length(find(LF<lamF))/opt.nperm;
        eval(['model.X', l,'.ANOVATP.pLam=pLamF;'])
        eval(['model.X', l,'.ANOVATP.TraceDistr=TF;'])
        pTrF=length(find(TF>trF))/opt.nperm;
        eval(['model.X', l,'.ANOVATP.pTr=pTrF;'])
        
        
        if strcmp(opt.plots, 'on')
            figure
            hist(SF, 100)
            hold on
            plot(ssqF, 0, 'or','MarkerFaceColor', 'r', 'Markersize', 18)
            title(['Effect of term X',l,' - p= ', num2str(pNullF)])
            xlabel('Sum of squares of Effect Matrix'), ylabel('Frequency')
            axis tight
            shg
            
            figure
            hist(RF, 100)
            hold on
            plot(resF, 0, 'or','MarkerFaceColor', 'r', 'Markersize', 18)
            title(['Effect of term X',l,' - p= ', num2str(pResF)])
            xlabel('Sum of squares of Y Residuals'), ylabel('Frequency')
            axis tight
            shg
            
            figure
            hist(LF, 100)
            hold on
            plot(lamF, 0, 'or','MarkerFaceColor', 'r', 'Markersize', 18)
            title(['Effect of term X',l,' - p= ', num2str(pLamF)])
            xlabel('Wilks'' Lambda'), ylabel('Frequency')
             axis tight
            shg
            
            figure
            hist(TF, 100)
            hold on
            plot(trF, 0, 'or','MarkerFaceColor', 'r', 'Markersize', 18)
            title(['Effect of term X',l,' - p= ', num2str(pTrF)])
            xlabel('Rao''s Trace'), ylabel('Frequency')
            axis tight
            shg
        end
        
        
        
    end
    

    if strcmp(opt.bootstrap, 'on')
        sl=(0.95+1)/2;
        ll=1-sl;
        svars=cell(1,size(mF.TPXloadings,2));
        
        
        Pboot=zeros([opt.nboot size(mF.TPXloadings)]);
        for k=1:opt.nboot
            [Xb,Yb]=makebootsample(Xx,Yf);
            mb=pls2cvtp(Xb,Yb,mF.nf,opt.pretr,[],[],opt.TPproj, opt.cv);
            [~,Pboot(k,:,:)]=orth_proc(mF.TPXloadings,mb.TPXloadings);
        end
        
        Pboot=sort(Pboot);
        Pbcrit=Pboot([ceil(ll*opt.nboot) ceil(sl*opt.nboot)],:,:);
        
        for iii=1:length(svars)
            svars{iii}=find(sign(squeeze(Pbcrit(1,:,iii).*Pbcrit(2,:,iii)))==1);
        end
        
        switch opt.bootsave
            case 'all'
                eval(['model.X', l, '.ANOVATP.Bootstrap.Loadings=Pboot; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.ConfIntervals=Pbcrit; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.SignificantVariables=svars; '])
            case 'confint'
                eval(['model.X', l, '.ANOVATP.Bootstrap.Loadings=[]; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.ConfIntervals=Pbcrit; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.SignificantVariables=svars; '])
            case 'signvars'
                eval(['model.X', l, '.ANOVATP.Bootstrap.Loadings=[]; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.ConfIntervals=[]; '])
                eval(['model.X', l, '.ANOVATP.Bootstrap.SignificantVariables=svars; '])
        end
        
        if strcmp(opt.plots, 'on')
            for kkk=1:length(svars)
                figure
                plot(1:size(mF.TPXloadings,1),mF.TPXloadings(:,kkk),'-r','linewidth', 3)
                hold on
                plot(1:size(mF.TPXloadings,1),squeeze(Pbcrit(:,:,kkk)),'--b','linewidth', 1.5)
                 axis tight
                
                title(['ANOVATP model of term X',l])
                xlabel('Variable index'), ylabel(['Loadings on TP', num2str(kkk)])
                
                shg
                
            end
        end
        
        
        
    end
    
    
    
end

model.TermLabels=deslabels;
model.Options=opt;

%%%%%%%%%%%%%%%%%%%%%%
function dmain=gendmat(designmat)
ns=size(designmat,1);
nfact=size(designmat,2);
dmain=cell(1,nfact);

for i=1:nfact
    lev=unique(designmat(:,i));
    nl=length(lev);
    dmat=zeros(ns,nl-1);
    for j=1:nl-1
        dmat(designmat(:,i)==lev(j),j)=1;
    end
    dmat(designmat(:,i)==lev(nl),:)=-1;
    dmain{i}=dmat;
end
%%%%%%%%%%%%%%%%%%%%%%%%
function [dmatrices,desterms, deslabels, desorder]=createdesign(designmat)
nfact=length(designmat);
ns=size(designmat{1},1);


indmat=fullfact(repmat(2,1,nfact));
nmat=size(indmat,1);
dmatrices=cell(1,nmat);
desterms=cell(1,nmat);
deslabels=cell(1,nmat);
desorder=zeros(nmat,1);



for i=1:nmat
    dm=ones(ns,1);
    for j=1:nfact
        if indmat(i,j)==1
            effmat=designmat{j};
        else
            effmat=ones(ns,1);
        end
        dm=kron(dm,effmat);
        dm=dm(1:ns+1:end,:);
    end
    dmatrices{i}=dm;
    desterms{i}=find(indmat(i,:)==1);
    deslabels{i}=char(64+find(indmat(i,:)==1));
    desorder(i)=length(desterms{i});
    
    
end

deslabels=sort(char(deslabels),2);

%Sorting according to increasing order of interactions
[deslabels, newindex]=sortrows(deslabels);

desterms=desterms(newindex);
dmatrices=dmatrices(newindex);
desorder=desorder(newindex);
deslabels=cellstr(deslabels)';
deslabels{1}='Mean';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Yd=createdesY(YY, ytype)

switch ytype
    case 'pca'
        YYc=YY-repmat(mean(YY), size(YY,1),1);
        [u,s,~]=svds(YYc, size(YY,2));
        Yd=u*s;
    case 'original'
        
        Yd=YY;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Xboot,Yboot]=makebootsample(X,Y)
bootp=zeros(size(X,1),1);
lev=unique(Y, 'rows');

for j=1:size(lev,1)
    xx=find(ismember(Y, lev(j,:), 'rows')==1);
    yy=ceil(length(xx)*rand(1,length(xx)));
    bootp(xx)=xx(yy);
end
Xboot=X(bootp,:);
Yboot=Y(bootp,:);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [r,yrot]=orth_proc(x,y)
%computes orthogonal procrustes rotation projecting matrix y onto the
%subspace spanned by matrix x.
% syntax: [r,yrot]=orth_proc(x,y)
% where r is the rotation matrix and yrot is the procrustes rotated y

[u,~,v]=svd(y'*x, 0);
r=u*v';
yrot=y*r;