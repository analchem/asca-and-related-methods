function plottpspecloads(factormodel, varargin)
% plottpspecloads Plots Spectral-like (line) ANOVA-TP loadings
%  The function plots ANOVATP loadings with their confidence itervals (it is
%  necessary that ANOVATP model is calculated using bootstrapping.
%  Significant variables are highlighted by a red trait in the plot.
%  Variable axis, variable axis name and which components to be plotted can
%  be input as well.
%  INPUTS:
%   factormodel = field of anova TP model corresponding to the design term
%  OPTIONAL INPUTS:
%           xax = Scale of the variable axis (if empty 1:nvars)
% .      tpcomp = Vector with the number of components to be plotted (if
%                 empty 1:ncomp).
%         axlab = Label for the Variable axis (if empty 'Variable Index')
%
%  I/O:
%       plottpspecloads(factormodel, xax, tpcomp, axlab);
%       plottpspecloads(factormodel, xax, tpcomp);
%       plottpspecloads(factormodel, xax, axlab);
%       plottpspecloads(factormodel, tpcomp, axlab);
%       plottpspecloads(factormodel, xax);
%       plottpspecloads(factormodel, axlab);
%       plottpspecloads(factormodel, tpcomp);
%       plottpspecloads(factormodel);
%
%
% Author: Federico Marini
% Version: 20/04/2020

[nv,ncomp]=size(factormodel.ANOVATP.Loadings);

if nargin==4
    
    xaxlab=varargin{3};
elseif nargin==3
    if ischar(varargin{2})
        if length(varargin{1})<nv
            xax=1:nv;
            tpcomp=varargin{1};
            xaxlab=varargin{2};
        else
            xax=varargin{1};
            tpcomp=1:ncomp;
            xaxlab=varargin{2};
        end
        
        
    else
        xax=varargin{1};
        tpcomp=varargin{2};
        xaxlab='Variable Index';
    end
    
elseif nargin==2
    if ischar(varargin{1})
        xax=1:nv;
        tpcomp=1:ncomp;
        xaxlab=varargin{1};
        
    elseif ~ischar(varargin{1}) && length(varargin{1})<nv
        tpcomp=varargin{1};
        xax=1:nv;
        xaxlab='Variable Index';
    else
        xax=varargin{1};
        tpcomp=1:ncomp;
        xaxlab='Variable Index';
    end
    
elseif nargin==1
    xax=1:nv;
    tpcomp=1:ncomp;
    xaxlab='Variable Index';
end




for i=1:length(tpcomp)
    figure
    ns=factormodel.ANOVATP.Bootstrap.SignificantVariables{tpcomp(i)};
    nns=1:nv; nns(ns)=[];
    hold on
    ls=factormodel.ANOVATP.Loadings(:,tpcomp(i));
    lns=ls;
    ls(nns)=NaN;
    lns(ns)=NaN;
    plot(xax, ls, 'r', 'linewidth', 2)
    plot(xax, lns, 'b', 'linewidth', 2)
    axis tight
    xx=xlim;
    plot(xx,[0 0], '--k', 'linewidth', 1.2)
    
    plot(xax,[squeeze(factormodel.ANOVATP.Bootstrap.ConfIntervals(1,:,tpcomp(i)));squeeze(factormodel.ANOVATP.Bootstrap.ConfIntervals(2,:,tpcomp(i)))], '--k', 'linewidth', 2);
    xlabel(xaxlab, 'fontsize', 12, 'fontweight', 'bold')
    ylabel(['TP Loadings on Component',num2str(tpcomp(i))], 'fontsize', 12, 'fontweight', 'bold')
end


